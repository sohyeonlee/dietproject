class NutritionController < ApplicationController
  include HTTParty
  
  def show 
    input = params['search']
    search = input.downcase.tr(" ", "_")
    base_url = "https://api.nutritionix.com/v1_1/search/"+search+"?results=0:20&fields=item_name,brand_name,item_id,nf_calories&appId=8b2998ce&appKey=4122e31c0d2142192cb7068931a3d5ac"
    res = HTTParty.get(base_url)
    @nutr = JSON.parse(res.body)
    @searched = input
  end
end
